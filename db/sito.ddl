-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Wed Dec 30 10:24:07 2020 
-- * LUN file: C:\xampp\htdocs\Progetto\db\sito.lun 
-- * Schema: sito3/1-1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database sito3;
use sito3;


-- Tables Section
-- _____________ 

create table ARTICOLO (
     ID_articolo int not null auto_increment,
     Nome varchar(50) not null,
     Tipo varchar(50) not null,
     Prezzo float(10) not null,
     Numero_rimasti int not null,
     constraint IDARTICOLO primary key (ID_articolo));

create table ARTICOLO_IN_ORDINE (
     ID_ordine int not null,
     ID_articolo int not null,
     Quantità int not null,
     constraint IDARTICOLO_IN_ORDINE primary key (ID_ordine, ID_articolo));

create table CARRELLO (
     ID_articolo int not null,
     Email varchar(50) not null,
     Quantità int not null,
     constraint IDCARRELLO primary key (ID_articolo, Email));

create table CONCERTO (
     Codice_biglietto int not null,
     Nome varchar(50) not null,
     Luogo varchar(100) not null,
     Data date not null,
     Ora int not null,
     constraint FKR_ID primary key (Codice_biglietto));

create table NOTIFICA (
     ID_notifica int not null auto_increment,
     Visualizzata char not null,
     Titolo varchar(50) not null,
     Descrizione varchar(100) not null,
     Email varchar(50) not null,
     ID_ordine int,
     Data_creazione datetime not null,
     constraint IDNOTIFICA primary key (ID_notifica));

create table ORDINE (
     ID_ordine int not null auto_increment,
     Data_arrivo date not null,
     Totale float(10) not null,
     Email varchar(50) not null,
     Stato int not null,
     constraint IDORDINE_ID primary key (ID_ordine));

create table UTENTE (
     Nome varchar(50) not null,
     Cognome varchar(50) not null,
     Email varchar(50) not null,
     Password char(64) not null,
     Paese varchar(50),
     Provincia varchar(50),
     Città varchar(50),
     Indirizzo varchar(50),
     Ultimo_controllo datetime,
     constraint IDUTENTE primary key (Email));

create table ARTICOLO_HOME (
     ID_articolo int not null auto_increment,
     Titolo varchar(50) not null,
     Testo varchar(1000) not null,
     constraint IDARTICOLO_HOME primary key (ID_articolo));

-- Constraints Section
-- ___________________ 

alter table ARTICOLO_IN_ORDINE add constraint FKcorrispondenza
     foreign key (ID_articolo)
     references ARTICOLO (ID_articolo);

alter table ARTICOLO_IN_ORDINE add constraint FKcom_ORD
     foreign key (ID_ordine)
     references ORDINE (ID_ordine);

alter table CARRELLO add constraint FKmet_ART
     foreign key (ID_articolo)
     references ARTICOLO (ID_articolo);

alter table CARRELLO add constraint FKmet_UTE
     foreign key (Email)
     references UTENTE (Email);

alter table CONCERTO add constraint FKR_FK
     foreign key (Codice_biglietto)
     references ARTICOLO (ID_articolo);

alter table NOTIFICA add constraint FKR
     foreign key (ID_ordine)
     references ORDINE (ID_ordine);

-- Not implemented
-- alter table ORDINE add constraint IDORDINE_CHK
--     check(exists(select * from ARTICOLO_IN_ORDINE
--                  where ARTICOLO_IN_ORDINE.ID_ordine = ID_ordine)); 

alter table ORDINE add constraint FKeffettua
     foreign key (Email)
     references UTENTE (Email);


-- Index Section
-- _____________ 

