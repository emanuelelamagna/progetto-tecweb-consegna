$(document).ready(function(){
    
    $("#categories").on("change", function(){
        if($("#categories").val()=="Accessori"){
            $(".musica").hide();
            $(".abbigliamento").hide();
            $(".accessori").show();
        } else if($("#categories").val()=="Tutti"){
            $(".accessori").show();
            $(".musica").show();
            $(".abbigliamento").show();
        } else if($("#categories").val()=="Abbigliamento"){
            $(".accessori").hide();
            $(".musica").hide();
            $(".abbigliamento").show();
        } else if($("#categories").val()=="Musica"){
            $(".accessori").hide();
            $(".musica").show();
            $(".abbigliamento").hide();
        }
    });

    $("#sorting").on("change", function(){
        window.location.href = "shop_index.php?sort=" + $("#sorting").val();
    });

    $(".delete").click(function(){
        var id = $(this).attr("id").split("delete_")[1];
        window.location.href = "template/delete_article.php?id=" + id;
    });

    $(".modify").click(function(){
        var id = $(this).attr("id").split("modify_")[1];
        window.location.href = "product_index.php?id=" + id;
    });

    $("main > div").click(function(){
        var id = $(this).attr("id");
        if(id != undefined) {
            id = parseInt(id);
            window.location.href = "product_index.php?id=" + id;
        }
    });

});