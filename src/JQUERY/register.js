$(document).ready(function(){

    $(window).keydown(function(event){  // Disabilito invio, così l'utente deve premere per forza il bottone.
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
    });

    $("#reg_form").submit(function(e){
        if ($("#name").val() == "" ||
            $("#surname").val() == "" ||
            $("#email").val() == "" ||
            $("#password").val() == "" ||
            $("#re-password").val() == "" ||
            $("#password").val() != $("#re-password").val()) {
            e.preventDefault();
            if ($("form + h3").length == 0) {
                $("form").after("<h3>Molto male, devi inserire le cose</h3>");
            }
        }
        return true;
    });

    $("#btn_register").click(function(e){
        e.preventDefault();
        $("#password").val(hashCode($("#password").val()));
        $("#re-password").val(hashCode($("#re-password").val()));
        $("form#reg_form").submit();
    });
});

function hashCode (str){        // Source: StackOverflow https://stackoverflow.com/questions/26057572/string-to-unique-hash-in-javascript-jquery
    str = String(str);
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}