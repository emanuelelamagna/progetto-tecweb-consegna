$(document).ready(function(){

    $("body > header > aside > button").click(function(){
        if ($("#menu").is(":visible")){
            $("#menu").fadeOut("fast", function(){
                $("main").fadeIn("fast");
                $("footer").fadeIn("fast");
             });
        } else {
            $("main").fadeOut("fast");
            $("footer").fadeOut("fast", function(){
                $("#menu").fadeIn("fast");
            });
        }
    });

    $("#profile_button").click(function(){
        window.location.href = "../PHP/profile_index.php";
    });

    $("#nm_button").click(function(){
        if(document.cookie.indexOf('night=')!=-1){
            $(this).children("img").attr("src", "../../img/utils/night.png");
            $(this).children("img").attr("alt", "night mode");
            $("body").css("background-image", "url('../../img/utils/sfondo.jpg')");
            document.cookie = "night=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        } else {
            $(this).children("img").attr("src", "../../img/utils/day.png");
            $(this).children("img").attr("alt", "day mode");
            $("body").css("background-image", "url('../../img/home/spazio.jpg')");
            document.cookie = "night=true; expires=Thu, 18 Dec 3000 12:00:00 UTC; path=/";
        }
    });


    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.responseText > 0){
                $("#btn_menu").css("background-color","red");
                $("#notifiche").text("NOTIFICHE(" + this.responseText + ")");
            }
            else{
                $("#btn_menu").css("background-color","white");
                $("#notifiche").text("NOTIFICHE");
            }
        }
    };
    xhttp.open("GET", "countNotifications.php", true);
    xhttp.send();
    setInterval(function(){
        xhttp.open("GET", "countNotifications.php", true);
        xhttp.send();
    }, 3000);

});