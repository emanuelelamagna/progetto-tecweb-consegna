$(document).ready(function(){

	$("#quantita").click(function(){
		var prezzo = parseFloat($("#prezzo").html().split("€")[1]);

        var new_quantity = $(this).val();

        $("#importo").html("Importo totale: €" + (prezzo*new_quantity).toFixed(2));
	});

});