$(document).ready(function(){

    $("td > button").click(function(){

        // Bottone "cambia stato" cliccato
        if ($(this).attr("id").split("change_").length > 1) {

            var id_ordine = $(this).attr("id").split("change_")[1];
            console.log(id_ordine);

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    window.location.href = "orders_index.php";
                }
            };
            xhttp.open("GET", "../PHP/updateOrder.php?id=" + id_ordine, true);
            xhttp.send();
        }
        // Bottone "vedi ordine" cliccato
        else {
            window.location.href = "order_index.php?id=" + $(this).attr("id");
        }
    });

});