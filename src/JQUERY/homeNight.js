$(document).ready(function(){

    $("#query").hide();

    $("#nm_button").click(function(){
        if(document.cookie.indexOf('night=')!=-1){
            $("section > img").attr("src", "../../img/utils/aereo-no-rainbow.png");
            $("section > img").attr("src", "../../img/utils/aereo-no-rainbow.png");
            $("section > img").css("height", "70%");
            $("section > img").css("width", "70%");
            if (window.matchMedia('(min-width: 769px)').matches) {
                $("main > section").css("padding", "0 5%");
                $("main > section > img").css("width", "41.4%");
                $("main > section > img").css("height", "100%");
            }
        } else {
            $("section > img").attr("src", "../../img/utils/miniaereologo.png");
            $("section > img").css("height", "100%");
            $("section > img").css("width", "100%");
            if (window.matchMedia('(min-width: 769px)').matches) {
                $("main > section").css("padding", "0 5%");
                $("main > section > img").css("width", "41.4%");
                $("main > section > img").css("height", "100%");
            }
        }
    });

    $(".modifica").click(function(e){
        let btn = e.target;
        let id = $(btn).parent().attr("id");
        let val = $(btn).html();
        if(val == "Modifica"){
            $("#" + id + " > h2").replaceWith("<input id=title type=text value=\"" + $("#" + id + " > h2").html() + "\"/>");
            $("#" + id + " > p").replaceWith("<input id=content type=text value=\"" + $("#" + id + " > p").html() + "\"/>");
            $(btn).html("Salva");
        } else if (val == "Salva"){
            $("#" + id + " > input#title").replaceWith("<h2>" + $("#" + id + " > input#title").val() + "</h2>");
            $("#" + id + " > input#content").replaceWith("<p>" + $("#" + id + " > input#content").val() + "</p>");
            $(btn).html("Modifica");
            $(btn).parent().children().prop("disabled", true);
            $.ajax({
              type: "POST",
              url: "updateHomeArticle.php",
              data: {query: "update", id: id, title: $("#" + id + " > h2").html(), content: $("#" + id + " > p").html()},
              success: function(){
                $(btn).parent().children().prop("disabled", false);
              },
              dataType: "text"
            });
        }
    });

    $(".elimina").click(function(e){
        let btn = e.target;
        let id = $(btn).parent().attr("id");
        $.ajax({
              type: "POST",
              url: "updateHomeArticle.php",
              data: {query: "delete", id: id},
              success: function(){
                $(btn).parent().remove();
              },
              dataType: "text"
            });
    });

});