<?php
    require_once("bootstrap.php");

    $templateParams["titolo"] = "I Santini - Home";
    $templateParams["page"] = "home.php";
    if(!isset($_COOKIE["night"])):
        $templateParams["name"] = "home";
    else:
        $templateParams["name"] = "homeNight";
    endif;
    
    require("template/base.php");
?>