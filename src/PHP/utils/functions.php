<?php
    function isUserLoggedIn(){
        return !empty($_SESSION["email"]);
    }

    function registerLoggedUser($user){
        $_SESSION["email"] = $user["Email"];
    }

    function isAdmin() {
    	return !empty($_SESSION) && $_SESSION["email"] == "admin@tecweb.it";
    }

    function saltAndCryptPassword($pass){
    	return hash("sha256", "SomeSaltynessForSecurity!1!1!11!!!1!, *é°ç;:_?^ç°!£)/&(£)!£/%(&=)(/?^éç:" . $pass . "Some other saltyness ééé%£%é£ç%éç/é/çé)(=é(°(/:*:/(52.468468224682486498493894564298249377298498484----6418498");
    }
?>