<?php
    require_once("bootstrap.php");

    if ($_SESSION["email"] == "admin@tecweb.it") {
        $templateParams["titolo"] = "I Santini - Ordini Admin";
        $templateParams["page"] = "admin_orders.php";
        $templateParams["name"] = "admin_orders";
    }
    else {
        $templateParams["titolo"] = "I Santini - Ordini";
        $templateParams["page"] = "orders.php";
        $templateParams["name"] = "orders";
    }

    require("template/base.php");
?>