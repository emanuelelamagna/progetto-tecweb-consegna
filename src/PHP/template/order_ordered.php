<?php

    $order_valid = $dbh -> checkOrder();
    if ($order_valid){
        $payment_received = true;
        if($payment_received){
            $_SESSION["DATA_ARRIVO"] = date("Y-m-d", strtotime("+15 day"));
            $idOrd = $dbh->createOrder();
            $dbh -> fillOrder($idOrd);
            $dbh -> emptyCart();
            echo "<h1>ORDINE AVVENUTO CON SUCCESSO</h1>";   
        } else {
            echo "<h1>ORDINE NON AVVENUTO</h1>";
            echo "<p>Non è stato possibile completare l'operazione di pagamento, controllare lo stato della carta.</p>";
        }
    } else {
        echo "<h1>ORDINE NON AVVENUTO</h1>";
        echo "<p>Alcuni articoli non sono più disponibili nelle quantità che desideri.</p>";
    }

?>
<p><button id="back_home">Torna alla home</button></p>