<div>
    <img src="../../img/band/rotella.png" alt="Rotella" />
    <div>
        <ul>
            <li>Nome: Pietro</li>
            <li>Nome d'arte: Rotella</li>
            <li>Ruolo: Cantante</li>
            <li>Descrizione: Un patato simpatico</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/nocca.png" alt="Nocca" />
    <div>
        <ul>
            <li>Nome: Thomas</li>
            <li>Nome d'arte: Nocca</li>
            <li>Ruolo: Corista</li>
            <li>Descrizione: Un patato alto</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/manolete.png" alt="Pedoni" />
    <div>
        <ul>
            <li>Nome: Mario</li>
            <li>Nome d'arte: Manolete</li>
            <li>Ruolo: Chitarrista</li>
            <li>Descrizione: Un patato magico</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/manona.png" alt="Pedoni" />
    <div>
        <ul>
            <li>Nome: Diego</li>
            <li>Nome d'arte: Signor Manona</li>
            <li>Ruolo: Bassista</li>
            <li>Descrizione: Un patato silenzioso</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/sementa.png" alt="Pedoni" />
    <div>
        <ul>
            <li>Nome: Enrico</li>
            <li>Nome d'arte: Seme</li>
            <li>Ruolo: Tastierista</li>
            <li>Descrizione: Un patato strano</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/gerundio.png" alt="Pedoni" />
    <div>
        <ul>
            <li>Nome: Gabriele</li>
            <li>Nome d'arte: Gerundio</li>
            <li>Ruolo: Narratore</li>
            <li>Descrizione: Un patato stronzo</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/pedoni.png" alt="Pedoni" />
    <div>
        <ul>
            <li>Nome: Emanuele</li>
            <li>Nome d'arte: Pedoni</li>
            <li>Ruolo: Corista</li>
            <li>Descrizione: Un patato bello</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/landan.png" alt="Pedoni" />
    <div>
        <ul>
            <li>Nome: Alessandro</li>
            <li>Nome d'arte: Landan</li>
            <li>Ruolo: Corista</li>
            <li>Descrizione: Un patato indie</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/gisto.png" alt="Pedoni" />
    <div>
        <ul>
            <li>Nome: Daniel</li>
            <li>Nome d'arte: Gisto</li>
            <li>Ruolo: Batterista</li>
            <li>Descrizione: Un patato confuso</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>
<div>
    <img src="../../img/band/mcfabbrone.png" alt="Pedoni" />
    <div>
        <ul>
            <li>Nome: Luca</li>
            <li>Nome d'arte: MC Fabbrone</li>
            <li>Ruolo: Corista</li>
            <li>Descrizione: Un patato fisicotto</li>
            <li>Tratti particolari: nessuno</li>
            <li>Prezzo: 5€ a notte</li>
        </ul>
    </div>
    <footer>
        <button type="button">Ordinalo subito</button>
    </footer>
</div>