<article id="imgsec">
    <h2>Foto profilo</h2>
	<?php if(file_exists("../../img/profile/".$_SESSION["email"].".jpeg")): ?>
    	<img src="../../img/profile/<?php echo $_SESSION["email"]?>.jpeg" alt="foto profilo" />
    <?php else: ?>
    	<img src="../../img/fotobase.png" alt="foto profilo" />
    <?php endif; ?>
</article>
<article id="infos">
    <h2>Informazioni</h2>
    <div>
        <label>Nome: </label>
        <p><?php echo $dbh->getSingleInfo("Nome")?></p>
    </div>
    <div>
        <label>Cognome: </label>
        <p><?php echo $dbh->getSingleInfo("Cognome")?></p>
    </div>
    <div>
        <label>Email: </label>
        <p><?php echo $dbh->getSingleInfo("Email")?></p>
    </div>
    <div>
        <label>Paese: </label>
        <p><?php
                if (empty($dbh->getSingleInfo("Paese"))):
                    echo "N/A";
                else:
                    echo $dbh->getSingleInfo("Paese");
                endif;
            ?></p>
    </div>
    <div>
        <label>Provincia: </label>
        <p><?php
                if (empty($dbh->getSingleInfo("Provincia"))):
                    echo "N/A";
                else:
                    echo $dbh->getSingleInfo("Provincia");
                endif;
            ?></p>
    </div>
    <div>
        <label>Città: </label>
        <p><?php
                if (empty($dbh->getSingleInfo("Città"))):
                    echo "N/A";
                else:
                    echo $dbh->getSingleInfo("Città");
                endif;
            ?></p>
    </div>
    <div>
        <label>Indirizzo: </label>
        <p><?php
                if (empty($dbh->getSingleInfo("Indirizzo"))):
                    echo "N/A";
                else:
                    echo $dbh->getSingleInfo("Indirizzo");
                endif;
            ?></p>
    </div>
</article>
<p>
    <button id="modify" type="button">Modifica informazioni</button>
</p>
<p>
    <button id="view_orders" type="button">Vedi i tuoi ordini</button>
</p>
<?php if (isUserLoggedIn()): ?>
    <p>
        <button id="logout" type="button">
            <img src="../../img/utils/logout.png" alt="logout" />
        </button>
    </p>
<?php endif; ?>