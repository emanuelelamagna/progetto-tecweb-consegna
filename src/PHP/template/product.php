<article>
    <h3>Title</h3>

    <?php $product = $dbh->getProduct($_GET["id"])[0] ?>

    <img src="../../img/shop/<?php echo lcfirst($product["Tipo"]) ?>/<?php echo str_replace(' ', '_', $product["Nome"]); ?>1.jpeg" alt="<?php echo $product["Nome"] ?> image"/>
    <p id="titolo"><?php echo $product["Nome"] ?></p>
    <?php if(!isAdmin()): ?>
        <p id="prezzo">€<?php echo number_format($product["Prezzo"], 2, '.', '') ?> al pezzo</p>
    <?php endif; ?>

    <?php if (isAdmin() || intval($product["Numero_rimasti"]) <= 10): ?>
        <p id="rimasti">Rimanenti: <?php echo intval($product["Numero_rimasti"]); ?></p>
    <?php endif; ?>
    
    <form action="product_index.php?id=<?php echo $_GET["id"]?>" method="post">
        <?php if(isAdmin()): ?>
            <p>
                <label for="stepper">€ al pezzo: </label>
                <input id="stepper" type="number" step="0.5" name="price" min="0.5" value="<?php echo $product["Prezzo"]; ?>"/>
            </p>
        <?php endif; ?>
        <p>
            <label for="quantita">Quantità: </label>
            <?php if(isAdmin()): ?>
                <input id="quantita" type="number" step="1" name="quantita" min="0" value="<?php echo $product["Numero_rimasti"]; ?>"/>
            <?php else: ?>
                <input id="quantita" type="number" step="1" name="quantita" min="1" <?php echo "max=\"".$product["Numero_rimasti"]."\""; ?> value="1"/>
            <?php endif; ?>
        </p>
        <?php if(!isAdmin()): ?>
            <p id="importo">Importo totale: €<?php echo number_format($product["Prezzo"], 2, '.', '') ?></p>
        <?php endif; ?>
        <button type="submit"><?php if(isAdmin()) { 
                                    echo "Modifica articolo";
                                }
                                else {
                                    echo "Aggiungi al carrello";
                                } ?></button>
    </form>

    <?php
        if(isAdmin() && isset($_POST["quantita"]) && isset($_POST["price"])) {
            $dbh->modifyProduct($_GET["id"], $_POST["quantita"], $_POST["price"]);
            header("Location: product_index.php?id=".$_GET["id"]);
            die();
        }
        else if(isset($_POST["quantita"]) && intval($_POST["quantita"])>0) {
            if(isUserLoggedIn()) {
                $dbh->addIntoCart($_SESSION["email"], $product["ID_articolo"], $_POST["quantita"]);
                header("Location: cart_index.php");
                die();
            }
            else {
                header("Location: login_index.php");
                die();
            }
        } 
    ?>

</article>