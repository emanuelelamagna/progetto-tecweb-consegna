<section>
    <h2>Login</h2>
    <h3>Inserisci username e password</h3>

    <form action="login_index.php" method="post" id="log_form">
        <p>
            <label for="email">Email</label>
            <input type="text" name="email" id="email" />
        </p>
        <p>
            <label for="password">Password</label>
            <input type="password" name="pass" id="password" />
        </p>
        <p>
            <button id="btn_login">Login</button>
        </p>
    </form>

    <?php if(isset($_POST["email"]) && isset($_POST["pass"])):
        $login_result = $dbh->checkLogin($_POST["email"], saltAndCryptPassword($_POST["pass"]));
        if(count($login_result)==0){
            //Login fallito
            echo "<p id=\"errorelogin\">Login failed</p>";
        }
        else{
            registerLoggedUser($login_result[0]);
            if ($_POST["email"] == "admin@tecweb.it") {
                header("Location: orders_index.php");
            }
            else {
                header("Location: profile_index.php");
            }
            die();
        }
    endif;?>

    <p>oppure</p>
    <p>
        <button type="button" id="btn_reg_log">Registrati</button>
    </p>
</section>