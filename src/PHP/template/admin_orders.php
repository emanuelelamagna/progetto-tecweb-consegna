<?php
    $ordini = $dbh->getAllOrders();
    if (empty($ordini)):
        echo "<p id=\"noOrders\">Non ci sono ordini.</p>";
    else: 
?>

<table>
    <caption>Ordini dell'admin</caption>
    <thead>
        <tr>
            <th id="mailacq" scope="col">Email acquirente</th>
            <th id="idord" scope="col">ID Ordine</th>
            <th id="stato" scope="col">Stato</th>
            <th id="cambiastato" scope="col">Cambia stato</th>
            <th id="vediord" scope="col">Vedi ordine</th>
        </tr>
    </thead>
    <tbody>

<?php
    $stati = array(
        1 => "In attesa di conferma",
        2 => "Confermato",
        3 => "Spedito",
        4 => "Consegnato"
    );
    foreach ($ordini as $ordine) {
        echo "<tr>";

        echo "<td headers=\"mailacq\">" . $ordine["Email"] . "</td>";
        echo "<td headers=\"idord\">" . $ordine["ID_ordine"] . "</td>";

        echo "<td headers=\"stato\">";
        if (array_key_exists($ordine["Stato"], $stati)) {
            echo $stati[$ordine["Stato"]];
        }
        else {
            echo "Questo non doveva succedere =(";
            var_dump($ordine);
        }
        echo "</td>";
        
        if ($ordine["Stato"] < 4) {
            echo "<td headers=\"cambiastato\"><button id=\"change_" . $ordine["ID_ordine"] . "\">Passa a \"" .$stati[$ordine["Stato"]+1]. "\"</button></td>";
        }
        else {
            echo "<td headers=\"cambiastato\"></td>";
        }

        echo "<td headers=\"vediord\"><button id=\"" . $ordine["ID_ordine"] . "\">Vedi ordine</button></td>";

        echo "</tr>";
    }
?>
    </tbody>
</table>
<?php endif; ?>