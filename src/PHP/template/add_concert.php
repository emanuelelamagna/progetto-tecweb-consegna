<?php
    require_once("../bootstrap.php");
    
    $dbh->addProduct($_GET["name"], "Biglietti concerti", $_GET["price"], $_GET["quantity"]);
    $dbh->addConcert($_GET["date"], $_GET["hour"], $_GET["name"], $_GET["place"]);

    header("Location: ../concerts_index.php");
    die();
?>