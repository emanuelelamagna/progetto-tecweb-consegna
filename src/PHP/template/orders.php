<?php
    $ordini = $dbh->getAllMyOrders($_SESSION["email"]);
    if (empty($ordini)):
        echo "<div id=\"noOrders\">";
        echo "<p>Non hai ordini al momento.</p>";
        echo "<p><button id=\"go_shop\">Vai allo shop</button></p>";
        echo "</div>";
    else: 
?>

<table>
    <caption>Ordini del cliente</caption>
    <thead>
        <tr>
            <th id="idord" scope="col">ID ordine</th>
            <th id="data" scope="col">Data arrivo</th>
            <th id="tot" scope="col">Totale</th>
            <th id="stato" scope="col">Stato</th>
        </tr>
    </thead>
    <tbody>

<?php
    foreach ($ordini as $ordine) {
        echo "<tr>";
        echo "<td headers=\"idord\">" . $ordine["ID_ordine"] . "</td>";
        echo "<td headers=\"data\">" . $ordine["Data_arrivo"] . "</td>";
        echo "<td headers=\"tot\">" . $ordine["Totale"] . "</td>";
        echo "<td headers=\"stato\">";
        switch ($ordine["Stato"]) {
            case 1:
                echo "In attesa di conferma";
                break;
            case 2:
                echo "Confermato";
                break;
            case 3:
                echo "Spedito";
                break;
            case 4:
                echo "Consegnato";
                break;
            default:
                echo "Questo non doveva succedere =(";
                break;
        }
        echo "</td>";
        
        echo "</tr>";
    }
?>
    </tbody>
</table>
<?php endif; ?>