<table>
    <caption>Concerti</caption>
    <thead>
        <tr>
            <th id="data" scope="col">Data</th>
            <th id="ora" scope="col">Ora</th>
            <th id="nome" scope="col">Nome</th>
            <th id="luogo" scope="col">Luogo</th>
            <?php 
                if(isAdmin()):
                    echo "<th id=\"rimozione\" scope=\"col\"></th>"; 
                else:
                    echo "<th id=\"biglietti\" scope=\"col\">Biglietti</th>";
                endif;
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
            $concerts = $dbh->getAllConcerts();
            foreach ($concerts as $concert) {
                echo "<tr>";

                echo "<td headers=\"data\">" . $concert["Data"] . "</td>";
                echo "<td headers=\"ora\">" . $concert["Ora"] . "</td>";
                echo "<td headers=\"nome\">" . $concert["Nome"] . "</td>";
                echo "<td headers=\"luogo\">" . $concert["Luogo"] . "</td>";
                if(!isAdmin()){
                    if (intval($concert["Numero_rimasti"] > 0)) {
                        echo "<td  headers=\"rimozione\"><button id=\"buy_" . $concert["Codice_biglietto"] . "\">Disponibili</button></td>";
                    }
                    else {
                        echo "<td  headers=\"rimozione\">Terminati</td>";
                    }
                } else {
                    echo "<td  headers=\"biglietti\"><button class=\"delete\" id=\"delete_" . $concert["Codice_biglietto"] . "\">Cancella concerto</button></td>";
                }
                echo "</tr>";
            }
        ?>
    </tbody>
</table>

<?php if(isAdmin()): ?>
    <form action="template/add_concert.php">
        <table>
        <caption>Modifica concerti</caption>
            <tbody>
                <tr>
                    <td><label for="date">Data: </label></td>
                    <td><input type="date" id="date" name="date" min="<?php echo date("Y-m-d"); ?>" required /></td>
                </tr>
                <tr>
                    <td><label for="hour">Ora: </label></td>
                    <td><input type="number" id="hour" name="hour" value="20" min="0" max="23" required /></td>
                </tr>
                <tr>
                    <td><label for="name">Nome: </label></td>
                    <td><input type="text" id="name" name="name" required /></td>
                </tr>
                <tr>
                    <td><label for="place">Luogo: </label></td>
                    <td><input type="text" id="place" name="place" required /></td>
                </tr>
                <tr>
                    <td><label for="price">Prezzo unitario (€): </label></td>
                    <td><input class="stepper" type="number" step="0.5" name="price" id="price" min="0.5" value="1" required /></td>
                </tr>
                <tr>
                    <td><label for="quantity">Disponibili: </label></td>
                    <td><input class="stepper" type="number" step="1" name="quantity" id="quantity" min="0" value="1" required /></td>
                </tr>
                <tr>
                    <td colspan=2><input id="addcon" type="submit" value="Aggiungi concerto" /></td>
                </tr>
            </tbody>
        </table>
    </form>
<?php endif; ?>