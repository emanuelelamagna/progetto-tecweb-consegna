<?php
    require_once("bootstrap.php");
    if($_POST["query"] == "update"){
    	$dbh->updateHomeArticle($_POST["title"], $_POST["content"], $_POST["id"]);
    }
    if($_POST["query"] == "delete"){
    	$dbh->deleteHomeArticle($_POST["id"]);
    }
    if($_POST["query"] == "add"){
    	$dbh->addHomeArticle($_POST["title"], $_POST["content"]);
    	header('Location: index.php');
    	die();
    }
?>