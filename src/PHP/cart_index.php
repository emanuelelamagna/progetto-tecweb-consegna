<?php
    require_once("bootstrap.php");

    if(isUserLoggedIn()):
        $templateParams["titolo"] = "I Santini - Carrello";
        $templateParams["page"] = "cart.php";
        $templateParams["name"] = "cart";
        require("template/base.php");
    else:
        header("Location: login_index.php");
        die();
    endif;    
?>