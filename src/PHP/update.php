<?php
	require_once("bootstrap.php");

	if (empty($_GET["id"])) {
		die("ID non valido");
	}

	$email = $_SESSION["email"];

	if (intval($_GET["id"]) == -1) {
		$dbh->emptyCart();
	}
	else {
		if (empty($_GET["q"]) && intval($_GET["q"])!=0) {
			die("Quantità non valida");
		}

		$dbh->updateQuantity($_GET["id"], $_GET["q"]);

		echo $dbh->getTotalCart();
	}
?>